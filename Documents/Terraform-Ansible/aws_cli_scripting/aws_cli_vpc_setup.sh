#Create variables
vpc_cidr=172.16.0.0/16
vpc_name=youracetech_vpc_2
subnet_name=youracetech_sn_web_2
subnet_cidr=172.16.2.0/24
gateway_name=youracetech_gw_2
route_table_name=youracetech_web_rt_2
home=0.0.0.0/0
default_cidr=0.0.0.0/0
security_group_name=youracetech_web_sg_2
security_group_desc="Allow http, https, and ssh access from home"

#other variables
centos_7_ami_id=ami-0c2aba6c
instance_type=t2.micro
ssh_key_name=youracetech_key
instance_ip=172.16.2.101


#Create VPC and Store the ID
vpc_id=$(aws ec2 create-vpc --cidr-block $vpc_cidr --query Vpc.VpcId --output text)

#Name the VPC
aws ec2 create-tags --resources $vpc_id --tags Key=Name,Value=$vpc_name

#Record a value to a file (VPC ID) to file
echo "vpc_id=$vpc_id" >> state_file

#Create Subnet and Store the ID
subnet_id=$(aws ec2 create-subnet  --vpc-id $vpc_id --cidr-block $subnet_cidr --query Subnet.SubnetId  --output text)

#Name the Subnet
aws ec2 create-tags --resources $subnet_id --tags Key=Name,Value=$subnet_name

#Create Internet Gateway Storing the ID
gateway_id=$(aws ec2 create-internet-gateway --query InternetGateway.InternetGatewayId --output text)

#Set gateway Name
aws ec2 create-tags --resources $gateway_id --tags Key=Name,Value=$gateway_name

#Attach Internet Gateway to VPC
aws ec2 attach-internet-gateway --internet-gateway-id $gateway_id --vpc-id $vpc_id

#Create Route-Table and store ID
route_table_id=$(aws ec2 create-route-table --vpc-id $vpc_id --query RouteTable.RouteTableId --output text)

#Set Routing Table Name
aws ec2 create-tags  --resources $route_table_id  --tags Key=Name,Value=$route_table_name

#Associate Routing Table with Subnet and store association ID
rt_association_id=$(aws ec2 associate-route-table --route-table-id $route_table_id --subnet-id $subnet_id --query AssociationId --output text)

#Add default Route to Routing Table
aws ec2 create-route --route-table-id $route_table_id --destination $default_cidr --gateway-id $gateway_id --output text

#Create security Group and store ID
security_group_id=$(aws ec2 create-security-group --group-name "$security_group_name" --description "$security_group_desc" --vpc-id $vpc_id --query GroupId --output text)
echo "display $security_group_id"
#Create Inbound rule to allow inbound ssh from home
aws ec2 authorize-security-group-ingress --group-id $security_group_id --protocol tcp --port 22 --cidr $home
aws ec2 authorize-security-group-ingress --group-id $security_group_id --protocol tcp --port 80 --cidr $home
aws ec2 authorize-security-group-ingress --group-id $security_group_id --protocol tcp --port 443 --cidr $home


echo "subnet_id=$subnet_id" >> state_file
echo "gateway_id=$gateway_id" >> state_file
echo "route_table_id=$route_table_id" >> state_file
echo "rt_association_id=$rt_association_id" >> state_file
echo "security_group_id=$security_group_id" >> state_file



