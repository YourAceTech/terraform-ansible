source state_file

#Create variables
vpc_cidr=172.16.0.0/16
vpc_name=youracetech_vpc_2
subnet_name=youracetech_sn_web_2
subnet_cidr=172.16.2.0/24
gateway_name=youracetech_gw_2
route_table_name=youracetech_web_rt_2
home_cidr=198.217.0.0/16
default_cidr=default_cidr
security_group_name=youracetech_web_sg_2

#Create Elastic IP and store allocation ID
elastic_ip_allocation_id=$(aws ec2 allocate-address  --domain vpc  --query AllocationId  --output text)

#Discover and Record the Elastic IP Address
elastic_ip=$(aws ec2 describe-addresses \
                          --allocation-ids $elastic_ip_allocation_id \
                          --query Addresses[*].PublicIp \
                          --output text)

echo "elastic_ip_allocation_id=$elastic_ip_allocation_id" >> state_file
echo "elastic_ip=$elastic_ip" >> state_file




