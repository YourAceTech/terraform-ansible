source state_file

#create key-pair
aws ec2 create-key-pair --key-name youracetech_key

#other variables
centos_7_ami_id=ami-a58d0dc5
instance_type=t2.micro
ssh_key_name=youracetech_key
instance_ip=172.16.2.102

#Create EC2 Instance and store ID
#Force deletion of EBS disk when instances terminates - block-device-mappings
instance_id=$(aws ec2 run-instances \
         --image-id $centos_7_ami_id \
         --count 1 \
         --instance-type $instance_type \
         --block-device-mappings "DeviceName=/dev/sda1,Ebs={DeleteOnTermination=true}" \
         --key-name $ssh_key_name \
         --security-group-ids $security_group_id \
         --subnet-id $subnet_id \
         --private-ip-address $instance_ip \
         --user-data file://ec2_userdata.yml \
         --query 'Instances[*].InstanceId' \
         --output text)

aws ec2 create-tags --resources $instance_id --tags Key=Name,Value=web2-wp-youraetech

#Loop to wait for EC2 Instance to come up
while state=$(aws ec2 describe-instances \
                        --instance-id $instance_id \
                        --query 'Reservations[*].Instances[*].State.Name' \
                        --output text );\
      [[ $state = "pending" ]]; do
     echo -n '.' # Show we are working on something
     sleep 3s    # Wait three seconds before checking again
done

echo -e "\n$instance_id: $state"

#Associate Elastic IP with EC2 Instance storing the Address Association ID
addr_association_id=$(aws ec2 associate-address \
                           --instance-id $instance_id \
                           --allocation-id $elastic_ip_allocation_id \
                           --query AssociationId \
                           --output text)

