# Provider spcific
provider "aws" {
    region = "${var.aws_region}"
}

# Variables for VPC module
module "vpc_subnets" {
	source = "./modules/vpc_subnets"
	name = "youracetech"
	environment = "dev"
	enable_dns_support = true
	enable_dns_hostnames = true
	vpc_cidr = "172.16.0.0/16"
        public_subnets_cidr = "172.16.10.0/24,172.16.20.0/24"
        private_subnets_cidr = "172.16.30.0/24,172.16.40.0/24"
        azs    = "us-west-2a,us-west-2b"
}

module "ssh_sg" {
	source = "./modules/ssh_sg"
	name = "youracetech"
	environment = "dev"
	vpc_id = "${module.vpc_subnets.vpc_id}"
	source_cidr_block = "0.0.0.0/0"
}

module "web_sg" {
	source = "./modules/web_sg"
	name = "youracetech"
	environment = "dev"
	vpc_id = "${module.vpc_subnets.vpc_id}"
	source_cidr_block = "0.0.0.0/0"
}

module "elb_sg" {
	source = "./modules/elb_sg"
	name = "youracetech"
	environment = "dev"
	vpc_id = "${module.vpc_subnets.vpc_id}"
}

module "rds_sg" {
    source = "./modules/rds_sg"
    name = "youracetech"
    environment = "dev"
    vpc_id = "${module.vpc_subnets.vpc_id}"
    security_group_id = "${module.web_sg.web_sg_id}"
}

module "ec2key" {
	source = "./modules/ec2key"
	key_name = "youracetech_aws"
	public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCIamv/SGYotgQDMr1878AZMFqz9g0HqeUjiTd1BM0rnQPrCt15Q2w+Mx3/xxk2nfyTw8p28oOV7KP+mFDsuzoUrHgSSh+qaPgwMTM1/ZW1c0DHMl6igPJT24hPg++Ps++fH/VVy/3TOpCWrhvcMlM/JdOd32dYc4dCVtLNYI7CLOrm4kHH6zg2lcOy4c22j34KMyo7YiylTEvqMbKUuhFUDQBRpz9hqB/liHRb9AzkiHY/FC5QVymHffkTlFV4rl9hpBcoZ9CvBXCQuWwSrmVUtVVJrCaqSyqZ+BflaOXzFuncQInzq+piTLXxcHPTtjUklXCLog7g98yEgpMVrrxt" 
}

module "ec2" {
	source = "./modules/ec2"
	name = "youracetech"
	environment = "dev"
	server_role = "web"
	ami_id = "ami-7c22b41c"
	key_name = "${module.ec2key.ec2key_name}"
	count = "2"
	security_group_id = "${module.ssh_sg.ssh_sg_id},${module.web_sg.web_sg_id}"
	subnet_id = "${module.vpc_subnets.public_subnets_id}"
	instance_type = "t2.nano"
	user_data = "#!/bin/bash\napt-get -y update\napt-get -y install nginx\n"
}

module "rds" {
	source = "./modules/rds"
	name = "youracetech"
	environment = "dev"
	storage = "5"
	engine_version = "5.6.27"
	db_name = "wordpress"
	username = "root"
	password = "${var.rds_password}"
	security_group_id = "${module.rds_sg.rds_sg_id}"
	subnet_ids = "${module.vpc_subnets.private_subnets_id}"
}

module "elb" {
	source = "./modules/elb"
	name = "youracetech"
	environment = "dev"
	security_groups = "${module.elb_sg.elb_sg_id}"
	availability_zones = "us-west-2a,us-west-2b"
	subnets = "${module.vpc_subnets.public_subnets_id}"
	instance_id = "${module.ec2.ec2_id}"
}
