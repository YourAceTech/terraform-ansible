# HA Wordpress cluster in AWS with Terraform & Ansible
	
### Requirements:

- Terraform
- Ansible
- AWS admin access
* Note: aws_cli_scripting folder was purely for pre-testing the AWS Environment

### Tools Used:
```shell
ansible --version
ansible 2.2.1.0
  config file =/terraform-ansible/ansible/ansible.cfg
  configured module search path = Default w/o overrides

terraform version
Terraform v0.9.3
```

### AWS Identity Management

Before creating virtual machines in Amazon's cloud, we need to complete some required identity and authorization setup.

Create:
- the generation of two AWS access keys
- the generating of an SSH key pair for this user to remotely access various services and
instances

This user will be able to perform all tasks in this module.

Click: Assigned MFA device for MFA
 

### Terraform

Before using the terraform, we need to export `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` as environment variables:

```
export AWS_ACCESS_KEY_ID="xxxxxxxxxxxxxxxx"
export AWS_SECRET_ACCESS_KEY="yyyyyyyyyyyyyyyyyyyy"
```
To Generate and show an execution plan (dry run):
```
terraform plan
```
To Builds or makes actual changes in infrastructure:
```
terraform apply
```
To inspect Terraform state or plan:
```
terraform show
```
To destroy Terraform-managed infrastructure:
```
terraform destroy
```
Enter the following when prompted:
```
var.aws_region
  Enter a value: us-west-2

var.domain_name
  Enter a value: youracetech.com

var.rds_password
  Enter a value: testpassword
```

**Note**: Terraform stores the state of the managed infrastructure from the last time Terraform was run. Terraform uses the state to create plans and make changes to the infrastructure.

### NEXT STEPS after building HA Web Cluster

Go to secret folder, and download youracetech_aws.pem & youracetech_aws.pub using the links

login using private key:
```shell
ssh -i "youracetech_key.pem" ubuntu@<ip-address>.us-west-2.compute.amazonaws.com
```
Copy your public key into /home/ubuntu/.ssh/authorized_keys so that you can ssh into the machine (and use ansible) without using the private key.

### Ansible Role after Terraform Provisioning:

Once the Terraform will create all the resources over AWS, you can use the Ansible to install the wordpress over the EC2 instances

### To use the provided Role:
```shell
ansible-playbook site.yml -e@../secret/secure.yml -e@../terraform-aws/youracetech-dev.yml
```
or use this command if you are using encrypted file:
```shell
ansible-playbook site.yml -e@../secret/secure.yml -e@../terraform-aws/youracetech-dev.yml --ask-vault-pass
```
where `secure.yml` contains secure information while `youracetech-dev.yml` contain the dnsname of the RDS(in this example) and this file will create during the terraform execution and it's name based on the values of these variables:
- name
- environment 

**Note:** `terraform.py` is dynamic inventory created by [CiscoCloud](https://github.com/CiscoCloud/terraform.py)